import React from 'react';
import {ul,li} from 'react-bootstrap';
import { Link } from 'react-router-dom';
import{BrowserRouter as Router,Switch,Route} from 'react-router-dom';
import Animation from './Animation';
import Movie from './Movie';

export default  function Video() {

    
    return (
        <div >
            <br/>
            
            <p style={{textAlign:"left",marginLeft:100}}>
                <ul>
                    <li><a href=""><Link to={"/Video/Animation"}>Animation</Link></a></li>
                    <li><a href=""><Link to={"/Video/Movie"}>Movie</Link></a></li>
                </ul>
               
              
                 <Switch>
                      <Route exact path="/Video " > <h3>Please Select A Topic </h3></Route>
                      <Route   path="/Video/Animation"   component={Animation}/>
                      <Route   path="/Video/Movie"  component={Movie}/>
                     
                </Switch>
           </p>
               
        
        </div>

       

    )
}

let mystyle={
    marginleft: '20px',
    
    
    
};
