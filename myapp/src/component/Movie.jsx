import React from 'react';
import { Link } from 'react-router-dom';
import{BrowserRouter as Router,Switch,Route} from 'react-router-dom';
import Adventure from './Adventure';
import Comedy from './Comedy';
import Crime from './Crime';
import Documentary from './Documentary';
function Movie() {
    return (
        <div>
            
            <h3> <b>Movie Category</b> </h3>
            <ul>
                <li><a href=""><Link to={"/Video/Movie/Adventure"}>Adventure</Link></a></li>
                <li><a href=""><Link to={"/Video/Movie/Comedy"}>Comedy</Link></a></li>
                <li><a href=""><Link to={"/Video/Movie/Crime"}>Crime</Link></a></li>
                <li><a href=""><Link to={"/Video/Movie/Documentary"}>Documentary</Link></a></li>
            </ul>

           

            <Switch>
                     <Route exact path="/Video " > <h3>Please Select A Topic </h3></Route>
                     <Route   path="/Video/Movie/Adventure"  component={Adventure}/>
                     <Route   path="/Video/Movie/Comedy"  component={Comedy}/>
                     <Route   path="/Video/Movie/Crime"  component={Crime}/>
                     <Route   path="/Video/Movie/Documentary"  component={Documentary}/>
                    
               </Switch>
        </div>
    )
}

export default Movie
