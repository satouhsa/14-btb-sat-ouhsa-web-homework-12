import React from 'react';
import { Link } from 'react-router-dom';
import{BrowserRouter as Router,Switch,Route} from 'react-router-dom';
import Action from './Action';
import Romance from './Romance';
import Comedy from './Comedy';
export default function Animation(){
    
    return (
        <div style={mystyle}>
            
                <h3> <b>Animation Category</b> </h3>

                <ul>
                    <li><a href=""><Link to={"/Video/Animation/Action"}>Action</Link></a></li>
                    <li><a href=""><Link to={"/Video/Animation/Romance"}>Romance</Link></a></li>
                    <li><a href=""><Link to={"/Video/Animation/Comedy"}>Comedy</Link></a></li>
                </ul>

               

                <Switch>
                     <Route exact path="/Video " > <h3>Please Select A Topic </h3></Route>
                      <Route   path="/Video/Animation/Action"   component={Action}/>
                      <Route   path="/Video/Animation/Romance"  component={Romance}/>
                      <Route   path="/Video/Animation/Comedy"  component={Comedy}/>
                     
                </Switch>
               

        </div>
    )
}

let mystyle={
    marginleft: '20px',
    
    
    
};


