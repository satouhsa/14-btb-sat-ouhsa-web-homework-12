import React from 'react';
import{Navbar,Nav,Button,Form,FormControl} from 'react-bootstrap';
import {Link} from 'react-router-dom';

export default  function Menu() {
    return (
        <div>
            
            <Navbar bg="dark" variant="dark">
                <Navbar.Brand as= {Link} to="">React Router</Navbar.Brand>
                <Nav className="mr-auto">
                <Nav.Link as= {Link} to="/Home" >Home</Nav.Link>
                <Nav.Link as={Link} to="/Video">Video</Nav.Link>
                <Nav.Link as={Link} to="/Account">Account</Nav.Link>
                <Nav.Link as={Link} to="Auth">Auth</Nav.Link>
                </Nav>
                <Form inline>
                <FormControl type="text" placeholder="Search" className="mr-sm-2" />
                <Button variant="outline-info">Search</Button>
                </Form>
           </Navbar>

           
           
        </div>
       
    )
}




