import React from 'react';
import {Card} from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default  function Home() {
    return (


        <div>
            <br/>
            <br/>
            
            <div className="container">

                <div className="row">

                    <div className="col-md-3">


                    <Card style={{ width: '17rem' }}>
                            <Card.Img variant="top" src={require('./image/sam.jpg')} />
                            <Card.Body>
                                <Card.Title>Card Title</Card.Title>
                                <Card.Text>
                                Some quick example text to build on the card title and make up the bulk of
                              
                                </Card.Text>
                               
                                <a href=""><Link to={`/Post/1`}>see more</Link></a>
                            </Card.Body>
                     </Card>
                    </div>
                    <div className="col-md-3">


                    <Card style={{ width: '17rem' }}>
                            <Card.Img variant="top" src={require('./image/vb.jpg')} />
                            <Card.Body>
                                <Card.Title>Card Title</Card.Title>
                                <Card.Text>
                                Some quick example text to build on the card title and make up the bulk of
                                the card's 
                                </Card.Text>
                                <a href=""><Link to={`/Post/1`}>see more</Link></a>
                            </Card.Body>
                     </Card>
                    </div>
                    <div className="col-md-3">


                    <Card style={{ width: '17rem' }}>
                            <Card.Img variant="top" src={require('./image/u.png')}/>
                            <Card.Body>
                                <Card.Title>Card Title</Card.Title>
                                <Card.Text>
                                Some quick example text to build on the card title and make up the bulk of
                                the card's content.
                                yes safd faffgg dew fgr 
                                </Card.Text>
                                <a href=""><Link to={`/Post/1`}>see more</Link></a>
                            </Card.Body>
                     </Card> 
                    </div>
                    <div className="col-md-3">


                    <Card style={{ width: '17rem' }}>
                            <Card.Img variant="top" src={require('./image/ni.jpg')} />
                            <Card.Body>
                                <Card.Title>Card Title</Card.Title>
                                <Card.Text>
                                Some quick example text to build on the card title and make up the bulk of
                                the card's content.
                                </Card.Text>
                                <a href=""><Link to={`/Post/1`}>see more</Link></a>
                            </Card.Body>
                     </Card> 
                    </div>

                </div>
                <br/>
               

                <div className="row">

                        <div className="col-md-3">


                        <Card style={{ width: '17rem' }}>
                                <Card.Img variant="top" src={require('./image/fy.jpg')} />
                                <Card.Body>
                                    <Card.Title>Card Title</Card.Title>
                                    <Card.Text>
                                    Some quick example text to build on the card title and make up the bulk of
                                    the card's content.
                                    </Card.Text>
                                    <a href=""><Link to={`/Post/1`}>see more</Link></a>
                                </Card.Body>
                        </Card>
                        </div>
                        <div className="col-md-3">


                        <Card style={{ width: '17rem' }}>
                                <Card.Img variant="top" src={require('./image/yt.jpg')} />
                                <Card.Body>
                                    <Card.Title>Card Title</Card.Title>
                                    <Card.Text>
                                    Some quick example text to build on the card title and make up the bulk of
                                  
                                    </Card.Text>
                                    <a href=""><Link to={`/Post/1`}>see more</Link></a>
                                </Card.Body>
                        </Card>
                        </div>
                        <br/>
                        <div className="col-md-3">


                        <Card style={{ width: '17rem' }}>
                                <Card.Img variant="top" src={require('./image/yo.png')}/>
                                <Card.Body>
                                    <Card.Title>Card Title</Card.Title>
                                    <Card.Text>
                                    Some quick example text to build on the card title and make 
                                
                                    </Card.Text>
                                    <a href=""><Link to={`/Post/1`}>see more</Link></a>
                                </Card.Body>
                        </Card> 
                        </div>
                        <div className="col-md-3">


                        <Card style={{ width: '17rem' }}>
                                <Card.Img variant="top" src={require('./image/yu.jpg')} />
                                <Card.Body>
                                    <Card.Title>Card Title</Card.Title>
                                    <Card.Text>
                                    Some quick example text to build on the card title and make up the bulk of
                                    the card's 
                                    </Card.Text>
                                    <a href=""><Link to={`/Post/1`}>see more</Link></a>
                                </Card.Body>
                        </Card> 
                        </div>
                        

                        </div>
                        <br/>
                        <br/>

            </div>
        </div>
    )
}


