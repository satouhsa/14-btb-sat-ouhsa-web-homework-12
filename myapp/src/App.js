import React from 'react';

import './App.css';
import Menu from './component/Menu';
import ReactRouter from './component/ReactRouter';
import Home from './component/Home';
import Video from './component/Video';
import Account from './component/Account';
import Auth from './component/Auth';
import 'bootstrap/dist/css/bootstrap.min.css';
import{BrowserRouter as Router,Switch,Route} from 'react-router-dom';
import Post from './component/Post';
import Animation from './component/Animation';
import Welcome from './component/Welcome';




function App() {
  return (
    <div className="App">
     <Router>
        <Menu/>
          <Switch>
        
            <Route path='/' exact component={ReactRouter}/>
            <Route path='/Home' component={Home}/>
            <Route path='/Video' component={Video}/>
            <Route path='/Account' component={Account}/>
            <Route path='/Auth' component={Auth}/>
            <Route path='/Post/:id' component={Post}/>
          
            <Route path='/Welcome' component={Welcome}/>
           
           
          </Switch>
      </Router>
    </div>
  );
}

export default App;
